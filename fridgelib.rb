require 'xmlrpc/client'
require 'digest/md5'
require 'hmac-md5'

class FridgeClient
	NONCE_LENGTH = 20
	NONCE_CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
	
	def initialize(endpoint, name = nil)
		@fridge_name = name
		@xmlrpc = XMLRPC::Client.new_from_uri(endpoint)
	end
	
	def fridge_name()
		if !@fridge_name
			@fridge_name = call_get_fridge_name()
		end
		@fridge_name
	end
	
	def transfer(from_user, to_user, amount, password)
		snonce = call_generate_nonce(from_user, password)
		call_transfer(snonce, from_user, to_user, amount, password) #Returns balance
	end
	
	def get_stock(user = nil)
		if user.nil?
			puts "get_stock()"
			@xmlrpc.call('get_stock')
		else
			puts "get_stock(#{user})"
			@xmlrpc.call('get_stock', user)
		end
	end
	
	def get_stock_hash(user = nil)
		stock_list = get_stock(user)
		stock_hash = {}
		stock_list.each do |product|
			stock_hash[product['product_code']] = product
		end
		stock_hash
	end
	
	def get_fridges()
		@xmlrpc.call('get_fridges')
	end
	
	def get_user_info(user)
		@xmlrpc.call('get_user_info', user)
	end
	
	def get_user_log(user, password)
		snonce = call_generate_nonce(user, password)
		call_get_user_log(snonce, user, password)
	end
	
	def get_messages()
		@xmlrpc.call('get_messages')
	end
	
	#Given the name of a remote fridge which this fridge knows about, construct and return the appropriate FridgeClient
	def get_remote_fridge(fridge_name)
		fridges = get_fridges()
		if !(endpoint = fridges[fridge_name])
			raise "Unknown remote fridge #{fridge_name}"
		end
		FridgeClient.new(endpoint, fridge_name)
	end
	
	#order_lines should be an array of OrderLine objects
	def purchase(user, order_lines, password)
		items = order_lines.map {|order_line| {'code' => order_line.code, 'quantity' => order_line.quantity} }
		snonce = call_generate_nonce(user, password)
		call_purchase(snonce, user, items, password)
	end
	
	def topup(user, amount, password)
		snonce = call_generate_nonce(user, password)
		call_topup(snonce, user, amount, password) #Returns balance
	end
	
	def remote_purchase(purchase_fridge, user, order_lines, password)
		items = order_lines.map {|order_line| {'code' => order_line.code, 'quantity' => order_line.quantity} }
		snonce = call_generate_nonce(user, password)
		call_remote_purchase(snonce, purchase_fridge, user, items, password)
	end
	
	def remote_topup(topup_fridge, user, amount, password)
		snonce = call_generate_nonce(user, password)
		call_remote_topup(snonce, topup_fridge, user, amount, password) #Returns balance
	end
	
protected
	def call_generate_nonce(username, password)
		#Sort out the arguments
		timestamp = Time.now.to_i
		cnonce = makeNonce()
		requestHmac = HMAC::MD5.hexdigest(Digest::MD5.hexdigest(password), "#{cnonce},#{timestamp},#{username}")
		
		puts "generate_nonce(#{cnonce}, #{timestamp}, #{username}, #{requestHmac})"
		#Actually make the XMLRPC call
		result = @xmlrpc.call('generate_nonce', cnonce, timestamp, username, requestHmac)
		
		#Check the HMAC on the response
		snonce = result['nonce']
		if HMAC::MD5.hexdigest(Digest::MD5.hexdigest(password), "#{snonce},#{cnonce}") != result['hmac']
			raise 'Invalid HMAC on result of generate_nonce.'
		elsif snonce.empty?
			raise 'Empty nonce returned by generate_nonce.'
		end
		
		#Return server nonce
		snonce
	end
	
	#Utility method used by sign, to turn arbitrary data into a string from which to generate the signature
	def stringify(data)
		if data.class == Array
			data.map{|el| stringify(el) }.join(',')
		elsif data.class == Hash
			#Join values sorted by key
			data.sort.map{|keyval| stringify(keyval[1]) }.join(',')
		else
			data
		end
	end
	
	#Given a plaintext password and an array of parameters, generate the appropriate HMAC for a method call or for checking the signature on a method result
	def sign(password, snonce, data)
		string = snonce + "," + stringify(data)
		HMAC::MD5.hexdigest(Digest::MD5.hexdigest(password), string)
	end
	
	def call_transfer(snonce, from_user, to_user, amount, password)
		#Calculate HMAC
		requestHmac = sign(password, snonce, [from_user, to_user, amount])
		
		puts "transfer(#{snonce}, #{from_user}, #{to_user}, #{amount}, #{requestHmac})"
		#Actually make the XMLRPC call
		result = @xmlrpc.call('transfer', snonce, from_user, to_user, amount, requestHmac)
		
		#Check the HMAC on the response
		balance = result['balance']
		if sign(password, snonce, [balance]) != result['hmac']
			raise 'Invalid HMAC on result of transfer.'
		end
		
		#Return new balance of from_user after successful transfer
		balance
	end
	
	def call_purchase(snonce, user, items, password)
		#items should be of form like [{'code' => 'AA', 'quantity' => 2}, {'code' => 'BB', 'quantity' => 1}]
		#Calculate HMAC
		requestHmac = sign(password, snonce, [user, items])
		
		puts "purchase(#{snonce}, #{user}, #{items.inspect}, #{requestHmac})"
		#Actually make the XMLRPC call
		result = @xmlrpc.call('purchase', snonce, user, items, requestHmac)
		
		#Check the HMAC on the response
		balance = result['balance']
		order_total = result['order_total']
		if sign(password, snonce, [balance, order_total]) != result['hmac']
			raise 'Invalid HMAC on result of purchase.'
		end
		
		#Return new balance of user after successful purchase, and total price of the purchase
		[balance, order_total]
	end
	
	def call_topup(snonce, user, amount, password)
		#Calculate HMAC
		requestHmac = sign(password, snonce, [user, amount])
		
		puts "topup(#{snonce}, #{user}, #{amount}, #{requestHmac})"
		#Actually make the XMLRPC call
		result = @xmlrpc.call('topup', snonce, user, amount, requestHmac)
		
		#Check the HMAC on the response
		balance = result['balance']
		if sign(password, snonce, [balance]) != result['hmac']
			raise 'Invalid HMAC on result of topup.'
		end
		
		#Return new balance of user after successful topup
		balance
	end
	
	def call_get_user_log(snonce, user, password)
		#Calculate HMAC
		requestHmac = sign(password, snonce, user)
		
		#Actually make the XMLRPC call
		puts "get_user_log(#{snonce}, #{user}, #{requestHmac})"
		result = @xmlrpc.call('get_user_log', snonce, user, requestHmac)
		
		#Check the HMAC on the response
		transactions = result['transactions']
		if sign(password, snonce, [transactions]) != result['hmac']
			raise 'Invalid HMAC on result of get_user_log'
		end
		
		#Return transaction log
		transactions
	end
	
	def call_remote_purchase(snonce, purchase_fridge, user, items, password)
		#Calculate HMAC
		requestHmac = sign(password, snonce, [purchase_fridge, user, items])
		
		puts "remote_purchase(#{snonce}, #{purchase_fridge}, #{user}, #{items.inspect}, #{requestHmac})"
		#Actually make the XMLRPC call
		result = @xmlrpc.call('remote_purchase', snonce, purchase_fridge, user, items, requestHmac)
		
		#Check the HMAC on the response
		balance = result['balance']
		order_total = result['order_total']
		if sign(password, snonce, [balance, order_total]) != result['hmac']
			raise 'Invalid HMAC on result of remote_purchase.'
		end
		
		#Return new balance of user after successful purchase, and total price of the purchase
		[balance, order_total]
	end
	
	def call_remote_topup(snonce, topup_fridge, user, amount, password)
		#Calculate HMAC
		requestHmac = sign(password, snonce, [topup_fridge, user, amount])
		
		puts "remote_topup(#{snonce}, #{topup_fridge}, #{user}, #{amount}, #{requestHmac})"
		#Actually make the XMLRPC call
		result = @xmlrpc.call('remote_topup', snonce, topup_fridge, user, amount, requestHmac)
		
		#Check the HMAC on the response
		balance = result['balance']
		if sign(password, snonce, [balance]) != result['hmac']
			raise 'Invalid HMAC on result of remote_topup.'
		end
		
		#Return new balance of user after successful topup
		balance
	end
	
	def call_get_fridge_name()
		@xmlrpc.call('get_fridge_name')
	end
	
private
	#Utility method to construct a random nonce value
	def makeNonce()
		snonce = ''
		NONCE_LENGTH.times do
			el = rand(NONCE_CHARS.length)
			snonce += NONCE_CHARS[el..el]
		end
		snonce
	end
end

#Given an integer amount of money in cents, return a formatted string
def FridgeClient.format_money(amount)
	if amount < 0
		amount = -amount
		sign = '-'
	else
		sign = ''
	end
	sprintf('%s$%d.%02d', sign, amount / 100, amount % 100)
end

OrderLine = Struct.new('OrderLine', :code, :quantity)
