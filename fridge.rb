#!/usr/bin/env ruby

require 'fridgelib'

require 'pp'
require 'rubygems'
require 'ruport'
require 'highline/import'

FRIDGE_ENDPOINT = 'http://localhost/~andrew/fridgeweb/fridgeserver.php'

class Command
	attr_reader :name, :description
	attr_accessor :help_text
	@@commands = {}
	
	def initialize(name, description)
		@name = name
		@description = description
		@help_text = "Usage: #{$0} #{name}\n"
		@@commands[name] = self
	end
	
	def self.commands
		@@commands
	end
	
	def run(args)
		puts "Sorry, the #{@name} command is not yet implemented."
	end
end

Help = Command.new('help', 'print help about a command')
def Help.run(args)
	if args.empty?
		puts 'Available commands:'
		Command.commands.each do |name, command|
			puts "  #{command.name} - #{command.description}"
		end
	else
		if Command.commands.has_key?(args[0])
			command = Command.commands[args[0]]
			puts "  #{command.name} - #{command.description}"
			puts command.help_text if !command.help_text.nil?
		else
			$stderr.puts "No help for #{args[0]}"
		end
	end
end
Help.help_text = <<EOF
Usage: #{$0} help [<command name>]
If no command name is given a list of all available commands will be printed.
EOF

Balance = Command.new('balance', 'displays your current balance')
def Balance.run(args)
	if args.length == 1
		#Split first argument into user and possibly their home fridge
		at = args[0].rindex('@')
		if at.nil?
			#User only, assume local
			username = args[0]
			fridge = $client
		else
			#Fridge name given
			username = args[0][0..at - 1]
			fridge = $client.get_remote_fridge(args[0][at + 1..-1])
		end
	else
		$stderr.puts 'Wrong number of arguments to balance.'
		puts help_text
		return
	end
	
	#Prompt for password
	password = ask('Password: ') { |q| q.echo = false }
	
	balance = fridge.transfer(username, username, 0, password)
	puts FridgeClient.format_money(balance)
end
Balance.help_text = <<EOF
Usage: #{$0} balance <username>[@<remote fridge name>]
You will be prompted for your password.
EOF

List = Command.new('list', 'lists the local or remote fridge stock')
def List.run(args)
	user = fridge_name = nil
	if args.length == 0
		#No fridge, no user
	elsif args.length == 1
		at = args[0].rindex('@')
		if at.nil?
			#User only, assume local fridge
			user = args[0]
		else
			#Fridge name given
			fridge_name = args[0][at + 1..-1]
			if at > 0
				#Username given too
				user = args[0][0..at - 1]
			end
		end
	else
		$stderr.puts 'Wrong number of arguments to list.'
		puts help_text
		return
	end
	
	if fridge_name.nil?
		#Default to local fridge
		fridge = $client
	else
		#Remote fridge name specified
		fridge = $client.get_remote_fridge(fridge_name)
	end
	
	category_names = {}
	tables = Hash.new() {|hash, key| hash[key] = Ruport::Data::Table.new(:column_names => ['Code', 'Description', 'Price', 'Stock']) }
	fridge.get_stock(user).each do |product|
		category_names[product['category_order']] = product['category']
		tables[product['category_order']] << [product['product_code'], product['description'], FridgeClient.format_money(product['price']), product['in_stock']]
	end
	category_names.each do |category_order, category|
		puts "= #{category} ="
		puts tables[category_order]
	end
end
List.help_text = <<EOF
Usage: #{$0} list [<username>][@<remote fridge name>]
If a remote fridge name is given look it up, otherwise use the local fridge.
A username may be supplied to list the prices specific to that username, for example if there is a graduate discount.
EOF

Transfer = Command.new('transfer', 'transfers from one user to another')
def Transfer.run(args)
	if args.length == 3
		#Split first argument into user and possibly their home fridge
		at = args[0].rindex('@')
		if at.nil?
			#User only, assume local
			from_user = args[0]
			fridge = $client
		else
			#Fridge name given
			from_user = args[0][0..at - 1]
			fridge = $client.get_remote_fridge(args[0][at + 1..-1])
		end
		
		to_user = args[1]
		amount = args[2].to_i
	else
		$stderr.puts 'Wrong number of arguments to transfer.'
		puts help_text
		return
	end
	if amount <= 0
		$stderr.puts "Invalid amount '#{args[2]}' to transfer."
		puts help_text
		return
	end
	
	puts "About to transfer #{FridgeClient.format_money(amount)} from #{from_user} to #{to_user}."
	
	#Prompt for password
	password = ask("Password for #{from_user}: ") { |q| q.echo = false }
	
	balance = fridge.transfer(from_user, to_user, amount, password)
	puts "Successfully transferred #{FridgeClient.format_money(amount)} to #{to_user}; your balance is now #{FridgeClient.format_money(balance)}."
end
Transfer.help_text = <<EOF
Usage: #{$0} transfer <from username> <to username>[@fridge] <amount in cents>
You will be prompted for your password.
EOF

Purchase = Command.new('purchase', 'make a purchase')
def Purchase.run(args)
	if args.length < 2
		$stderr.puts "Insufficient arguments to purchase."
		puts help_text
		return
	end
	
	#Split first argument into user and possibly their home fridge
	at = args[0].rindex('@')
	if at.nil?
		#User only, assume local
		local_user = args[0]
	else
		#Fridge name given, assume remote user purchasing at local fridge
		local_user = home_fridge = args[0][at + 1..-1]
		remote_user = args[0][0..at - 1]
	end
	
	#Parse items in order
	order_lines = args[1..-1].map do |arg|
		parts = arg.split('*')
		if parts.length == 1
			OrderLine.new(parts[0].upcase, 1) #Quantity defaults to 1 if not given
		elsif parts.length == 2 && (quantity = parts[1].to_i) > 0
			OrderLine.new(parts[0].upcase, quantity)
		else
			$stderr.puts "Invalid item '#{arg}'."
			puts help_text
			return
		end
	end
	
	#Check how much this order will cost
	stock = $client.get_stock_hash(local_user)
	order_total = order_lines.inject(0) {|sum, order_line| sum + stock[order_line.code]['price'] * order_line.quantity }
	table = Ruport::Data::Table.new(:column_names => ['Code', 'Description', 'Price each', 'Quantity'])
	order_lines.each do |order_line|
		product = stock[order_line.code]
		table << [order_line.code, product['description'], FridgeClient.format_money(product['price'] * order_line.quantity), order_line.quantity]
	end
	puts "About to order:"
	puts table
	puts "Total price expected to be #{FridgeClient.format_money(order_total)}."
	
	#Ask for password (user may take this chance to abort), and make the order
	password = ask("Password for #{args[0]}: ") { |q| q.echo = false }
	if home_fridge.nil?
		balance, order_total = $client.purchase(local_user, order_lines, password)
	else
		#Remote fridge name specified
		home_client = $client.get_remote_fridge(home_fridge)
		balance, order_total = home_client.remote_purchase($client.fridge_name, remote_user, order_lines, password)
	end
	
	puts "Purchased for #{FridgeClient.format_money(order_total)}. Balance is now #{FridgeClient.format_money(balance)}."
end
Purchase.help_text = <<EOF
Usage: #{$0} purchase <username> <code>[*<quantity>] ...
You will be prompted for your password, and will be asked to confirm the purchase before it is made.
EOF

Topup = Command.new('topup', 'tops up a user\'s account')
def Topup.run(args)
	if args.length == 2
		user = args[0]
		amount = args[1].to_i
	else
		$stderr.puts 'Wrong number of arguments to topup.'
		puts help_text
		return
	end
	if amount == 0
		$stderr.puts "Invalid amount '#{args[1]}' to topup."
		puts help_text
		return
	end
	
	#Split first argument into user and possibly their home fridge
	at = user.rindex('@')
	if at.nil?
		#User only, assume local
		local_user = user
	else
		#Fridge name given, assume remote user topping up at local fridge
		home_fridge = user[at + 1..-1]
		remote_user = user[0..at - 1]
	end
	
	puts "About to topup #{user}'s balance by #{FridgeClient.format_money(amount)}."
	
	#Prompt for password
	password = ask("Password for #{user}: ") { |q| q.echo = false }
	
	if home_fridge.nil?
		balance = $client.topup(local_user, amount, password)
	else
		#Remote fridge name specified
		home_client = $client.get_remote_fridge(home_fridge)
		balance = home_client.remote_topup($client.fridge_name, remote_user, amount, password)
	end
	puts "Successfully topped up. #{user}'s balance is now #{FridgeClient.format_money(balance)}."
end
Topup.help_text = <<EOF
Usage: #{$0} topup <username> <amount in cents>
You will be prompted for your password.
EOF

History = Command.new('history', 'lists a user\'s transaction history')
def History.run(args)
	if args.length != 1
		$stderr.puts 'Wrong number of arguments to history.'
		puts help_text
		return
	end
	
	user = args[0]
	
	#Prompt for password
	password = ask("Password for #{user}: ") { |q| q.echo = false }
	
	user_log = $client.get_user_log(user, password)
	user_log = user_log.sort_by {|log_line| log_line['date_time'] }
	table = Ruport::Data::Table.new(:column_names => ['Date', 'Type', 'Item / Sender / Recipient', 'Price'])
	user_log.each do |log_line|
		if log_line['type'] == 'PURCHASE'
			table << [log_line['date_time'], 'PURCHASE', log_line['purchase_quantity'] == 1 ? log_line['product_code'] : "#{log_line['product_code']} * #{log_line['purchase_quantity']}", FridgeClient::format_money(log_line['amount'])]
		elsif log_line['type'] == 'CREDIT'
			table << [log_line['date_time'], 'CREDIT', nil, FridgeClient::format_money(log_line['amount'])]
		elsif log_line['type'] == 'XFER-OUT'
			table << [log_line['date_time'], 'TRANSFER', "to #{log_line['recipient']}", FridgeClient::format_money(log_line['amount'])]
		elsif log_line['type'] == 'XFER-IN'
			table << [log_line['date_time'], 'TRANSFER', "from #{log_line['sender']}", FridgeClient::format_money(log_line['amount'])]
		end
	end
	puts table
end
History.help_text = <<EOF
Usage: #{$0} history <username>
EOF

Info = Command.new('info', 'dislay information about the fridge')
def Info.run(args)
	messages = $client.get_messages()
	puts "#{$client.fridge_name} fridge"
	puts "#{messages['num_served']} served"
	puts messages['signup_info']
	puts "Naughty people:"
	messages['naughty_people'].each do |name|
		puts " #{name}"
	end
end

$client = FridgeClient.new(FRIDGE_ENDPOINT)

if ARGV.empty?
	Help.run([])
else
	command_name = ARGV[0]
	if Command.commands.has_key?(command_name)
		Command.commands[command_name].run(ARGV[1..-1])
	else
		$stderr.puts "Unknown command #{command_name}"
		Help.run([])
	end
end
